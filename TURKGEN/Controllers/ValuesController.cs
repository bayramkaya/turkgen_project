﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TURKGEN.Models;
using TURKGEN.Models.Response;
using TURKGEN.Models.Send;
using TURKGEN.Properties;
namespace TURKGEN.Controllers
{

    [Route("api")]
    public class ValuesController : ControllerBase
    {
        AppDbContext db = new AppDbContext();
        Constant cons = new Constant();
        [HttpGet]
        public string Get()
        {
            var pong = db.TArticle.Where(x=>x.IdcurrentStatus== (int)Constant.ItemStatus.STATUS_ACTIVE).Count();
            return "pong - " + pong.ToString();

        }
        /// <summary>
        /// Makale ekle/düzenle
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("ArticleTrans")]
        [HttpPost]
        public dynamic ArticleTrans([FromBody] ArticleResponseModel form)
        {
            ServicesStatus status = new ServicesStatus();
            dynamic result = null;
            TArticle Trans;
            if (form.Idarticle != 0)
            {
                Trans = db.TArticle.SingleOrDefault(x => x.Idarticle == form.Idarticle && x.IdcurrentStatus == (int)Constant.ItemStatus.STATUS_ACTIVE);
                Trans.UpdateDate = DateTime.Now;
                Trans.UpdateUser = form.IdTransUser;
                status.Message = "Makale güncellendi.";
                status.Code = 200;
            }
            else
            {
                Trans = new TArticle();
                Trans.CreateDate = DateTime.Now;
                Trans.CreateUser = form.IdTransUser;
                status.Message = "Makale eklendi.";
                status.Code = 200;
            }
            Trans.IdcurrentStatus = (int)Constant.ItemStatus.STATUS_ACTIVE;
            Trans.Title = form.Title;
            Trans.Detail = form.Detail;
            if (form.Idarticle == 0)
            {
                db.TArticle.Add(Trans);
            }
            db.SaveChanges();
            db.TArticlecategory.RemoveRange(db.TArticlecategory.Where(x => x.Idarticle == Trans.Idarticle));
            db.SaveChanges();
            foreach (var item in form.CategoryID)
            {
                TArticlecategory add = new TArticlecategory();
                add.Idcategory = item;
                add.Idarticle = Trans.Idarticle;
                db.TArticlecategory.Add(add);
                db.SaveChanges();
            }
            result = status;
            return result;
        }
        /// <summary>
        /// Makale sil methotu
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("ArticleDelete")]
        [HttpPost]
        public dynamic ArticleDelete([FromBody] ArticleResponseModel form)
        {
            dynamic result = null;
            ServicesStatus status = new ServicesStatus();
            try
            {
                TArticle delete = db.TArticle.SingleOrDefault(x => x.Idarticle == form.Idarticle && x.IdcurrentStatus == (int)Constant.ItemStatus.STATUS_ACTIVE);
                delete.IdcurrentStatus = (int)Constant.ItemStatus.STATUS_DELETED;
                delete.DeleteDate = DateTime.Now;
                delete.DeleteUser = form.IdTransUser;
                db.SaveChanges();
                status.Message = "Makale silindi";
                status.Code = 200;
            }
            catch (Exception ex)
            {
                status.Message = ex.Message;
                status.Code = 500;
            }
            result = status;
            return result;
        }

    }
}
