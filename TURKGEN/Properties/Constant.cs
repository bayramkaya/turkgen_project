﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TURKGEN.Models;

namespace TURKGEN.Properties
{
    public class Constant
    {
        public enum ItemStatus
        {
            STATUS_ACTIVE = 2,
            STATUS_SUSPENDED = 3,
            STATUS_DELETED = 4
        }       
    }

    public class ServicesStatus
    {
        public string Message { get; set; }
        public int Code { get; set; }
    }
}
