﻿using System;
using System.Collections.Generic;

namespace TURKGEN.Models
{
    public partial class TCategory
    {
        public int Idcategory { get; set; }
        public int? IdcurrentStatus { get; set; }
        public string CategoryName { get; set; }
        public int? Gravity { get; set; }
    }
}
