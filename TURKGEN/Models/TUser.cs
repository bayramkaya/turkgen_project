﻿using System;
using System.Collections.Generic;

namespace TURKGEN.Models
{
    public partial class TUser
    {
        public int Iduser { get; set; }
        public int IdcurrentStatus { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Token { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
