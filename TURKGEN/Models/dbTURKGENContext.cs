﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TURKGEN.Models
{
    public partial class dbTURKGENContext : DbContext
    {
        public dbTURKGENContext()
        {
        }

        public dbTURKGENContext(DbContextOptions<dbTURKGENContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TArticle> TArticle { get; set; }
        public virtual DbSet<TArticlecategory> TArticlecategory { get; set; }
        public virtual DbSet<TCategory> TCategory { get; set; }
        public virtual DbSet<TComment> TComment { get; set; }
        public virtual DbSet<TLookup> TLookup { get; set; }
        public virtual DbSet<TUser> TUser { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.;Database=dbTURKGEN;user id=sa; password=1234566");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TArticle>(entity =>
            {
                entity.HasKey(e => e.Idarticle);

                entity.ToTable("tARTICLE");

                entity.Property(e => e.Idarticle).HasColumnName("IDArticle");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DeleteDate).HasColumnType("datetime");

                entity.Property(e => e.IdcurrentStatus).HasColumnName("IDCurrentStatus");

                entity.Property(e => e.Title).HasMaxLength(150);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TArticlecategory>(entity =>
            {
                entity.HasKey(e => e.IdarticleCategory);

                entity.ToTable("tARTICLECATEGORY");

                entity.Property(e => e.IdarticleCategory).HasColumnName("IDArticleCategory");

                entity.Property(e => e.Idarticle).HasColumnName("IDArticle");

                entity.Property(e => e.Idcategory).HasColumnName("IDCategory");
            });

            modelBuilder.Entity<TCategory>(entity =>
            {
                entity.HasKey(e => e.Idcategory)
                    .HasName("PK_dbCATEGORY");

                entity.ToTable("tCATEGORY");

                entity.Property(e => e.Idcategory)
                    .HasColumnName("IDCategory")
                    .ValueGeneratedNever();

                entity.Property(e => e.CategoryName).HasMaxLength(30);

                entity.Property(e => e.IdcurrentStatus).HasColumnName("IDCurrentStatus");
            });

            modelBuilder.Entity<TComment>(entity =>
            {
                entity.HasKey(e => e.Idcomment);

                entity.ToTable("tCOMMENT");

                entity.Property(e => e.Idcomment)
                    .HasColumnName("IDComment")
                    .ValueGeneratedNever();

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DeleteDate).HasColumnType("datetime");

                entity.Property(e => e.IdcurrentStatus).HasColumnName("IDCurrentStatus");

                entity.Property(e => e.Iduser).HasColumnName("IDUser");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TLookup>(entity =>
            {
                entity.HasKey(e => e.Idlookup);

                entity.ToTable("tLOOKUP");

                entity.Property(e => e.Idlookup).HasColumnName("IDLookup");

                entity.Property(e => e.Code).HasMaxLength(150);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.IdcurrentStatus).HasColumnName("IDCurrentStatus");

                entity.Property(e => e.Idparent).HasColumnName("IDParent");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<TUser>(entity =>
            {
                entity.HasKey(e => e.Iduser);

                entity.ToTable("tUSER");

                entity.Property(e => e.Iduser).HasColumnName("IDUser");

                entity.Property(e => e.IdcurrentStatus).HasColumnName("IDCurrentStatus");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasMaxLength(45);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(30);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
