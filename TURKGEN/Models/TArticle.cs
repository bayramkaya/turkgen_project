﻿using System;
using System.Collections.Generic;

namespace TURKGEN.Models
{
    public partial class TArticle
    {
        public int Idarticle { get; set; }
        public int IdcurrentStatus { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? UpdateUser { get; set; }
        public DateTime? DeleteDate { get; set; }
        public int? DeleteUser { get; set; }
    }
}
