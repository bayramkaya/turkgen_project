﻿using System;
using System.Collections.Generic;

namespace TURKGEN.Models
{
    public partial class TLookup
    {
        public int Idlookup { get; set; }
        public int? IdcurrentStatus { get; set; }
        public int? Idparent { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CreateUser { get; set; }
    }
}
