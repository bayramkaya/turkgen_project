﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TURKGEN.Models.Response
{
    public class ArticleResponseModel
    {
        public int Idarticle { get; set; }
        public int IdTransUser { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }
        public List<int> CategoryID { get; set; }

    }
}
