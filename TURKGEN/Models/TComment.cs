﻿using System;
using System.Collections.Generic;

namespace TURKGEN.Models
{
    public partial class TComment
    {
        public int Idcomment { get; set; }
        public int IdcurrentStatus { get; set; }
        public int Iduser { get; set; }
        public string Comment { get; set; }
        public double? Stars { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? UpdateUser { get; set; }
        public DateTime? DeleteDate { get; set; }
        public int? DeleteUser { get; set; }
    }
}
