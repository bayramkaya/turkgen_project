USE [dbTURKGEN]
GO
/****** Object:  Table [dbo].[tARTICLE]    Script Date: 30.08.2020 23:02:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tARTICLE](
	[IDArticle] [int] IDENTITY(1,1) NOT NULL,
	[IDCurrentStatus] [int] NOT NULL,
	[Title] [nvarchar](150) NULL,
	[Detail] [nvarchar](max) NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteUser] [int] NULL,
 CONSTRAINT [PK_tARTICLE] PRIMARY KEY CLUSTERED 
(
	[IDArticle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tARTICLECATEGORY]    Script Date: 30.08.2020 23:02:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tARTICLECATEGORY](
	[IDArticleCategory] [int] IDENTITY(1,1) NOT NULL,
	[IDArticle] [int] NULL,
	[IDCategory] [int] NULL,
 CONSTRAINT [PK_tARTICLECATEGORY] PRIMARY KEY CLUSTERED 
(
	[IDArticleCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tCATEGORY]    Script Date: 30.08.2020 23:02:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCATEGORY](
	[IDCategory] [int] IDENTITY(1,1) NOT NULL,
	[IDCurrentStatus] [int] NULL,
	[CategoryName] [nvarchar](30) NULL,
	[Gravity] [int] NULL,
 CONSTRAINT [PK_dbCATEGORY] PRIMARY KEY CLUSTERED 
(
	[IDCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tCOMMENT]    Script Date: 30.08.2020 23:02:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCOMMENT](
	[IDComment] [int] IDENTITY(1,1) NOT NULL,
	[IDCurrentStatus] [int] NOT NULL,
	[IDUser] [int] NOT NULL,
	[Comment] [nvarchar](250) NOT NULL,
	[Stars] [float] NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteUser] [int] NULL,
 CONSTRAINT [PK_tCOMMENT] PRIMARY KEY CLUSTERED 
(
	[IDComment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tLOOKUP]    Script Date: 30.08.2020 23:02:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tLOOKUP](
	[IDLookup] [int] IDENTITY(1,1) NOT NULL,
	[IDCurrentStatus] [int] NULL,
	[IDParent] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[Code] [nvarchar](150) NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [int] NULL,
 CONSTRAINT [PK_tLOOKUP] PRIMARY KEY CLUSTERED 
(
	[IDLookup] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tUSER]    Script Date: 30.08.2020 23:02:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUSER](
	[IDUser] [int] IDENTITY(1,1) NOT NULL,
	[IDCurrentStatus] [int] NOT NULL,
	[Name] [nvarchar](25) NOT NULL,
	[Surname] [nvarchar](25) NOT NULL,
	[Token] [nvarchar](45) NOT NULL,
	[UserName] [nvarchar](30) NOT NULL,
	[Password] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_tUSER] PRIMARY KEY CLUSTERED 
(
	[IDUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tARTICLE] ON 

INSERT [dbo].[tARTICLE] ([IDArticle], [IDCurrentStatus], [Title], [Detail], [CreateDate], [CreateUser], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser]) VALUES (1, 2, N'DENEME', N'DENEME', CAST(N'2020-08-30 00:00:00.000' AS DateTime), 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tARTICLE] ([IDArticle], [IDCurrentStatus], [Title], [Detail], [CreateDate], [CreateUser], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser]) VALUES (2, 0, N'deneme', N'deneme', CAST(N'2020-08-30 22:06:51.213' AS DateTime), 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tARTICLE] ([IDArticle], [IDCurrentStatus], [Title], [Detail], [CreateDate], [CreateUser], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser]) VALUES (3, 2, N'deneme', N'deneme', CAST(N'2020-08-30 22:13:29.537' AS DateTime), 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tARTICLE] ([IDArticle], [IDCurrentStatus], [Title], [Detail], [CreateDate], [CreateUser], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser]) VALUES (4, 2, N'deneme', N'deneme', CAST(N'2020-08-30 22:14:27.470' AS DateTime), 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tARTICLE] ([IDArticle], [IDCurrentStatus], [Title], [Detail], [CreateDate], [CreateUser], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser]) VALUES (5, 2, N'deneme', N'deneme', CAST(N'2020-08-30 22:19:32.173' AS DateTime), 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tARTICLE] ([IDArticle], [IDCurrentStatus], [Title], [Detail], [CreateDate], [CreateUser], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser]) VALUES (6, 2, N'deneme', N'deneme', CAST(N'2020-08-30 22:33:46.473' AS DateTime), 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tARTICLE] ([IDArticle], [IDCurrentStatus], [Title], [Detail], [CreateDate], [CreateUser], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser]) VALUES (7, 2, N'deneme', N'deneme', CAST(N'2020-08-30 22:34:23.237' AS DateTime), 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tARTICLE] ([IDArticle], [IDCurrentStatus], [Title], [Detail], [CreateDate], [CreateUser], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser]) VALUES (8, 4, N'deneme', N'deneme', CAST(N'2020-08-30 22:36:59.993' AS DateTime), 1, CAST(N'2020-08-30 22:37:36.170' AS DateTime), 1, CAST(N'2020-08-30 22:48:41.393' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tARTICLE] OFF
SET IDENTITY_INSERT [dbo].[tARTICLECATEGORY] ON 

INSERT [dbo].[tARTICLECATEGORY] ([IDArticleCategory], [IDArticle], [IDCategory]) VALUES (3, 8, 4)
INSERT [dbo].[tARTICLECATEGORY] ([IDArticleCategory], [IDArticle], [IDCategory]) VALUES (4, 8, 6)
SET IDENTITY_INSERT [dbo].[tARTICLECATEGORY] OFF
SET IDENTITY_INSERT [dbo].[tLOOKUP] ON 

INSERT [dbo].[tLOOKUP] ([IDLookup], [IDCurrentStatus], [IDParent], [Name], [Code], [CreateDate], [CreateUser]) VALUES (1, 2, 0, N'CURRENT_STATUS', N'Durum', CAST(N'2020-05-21 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[tLOOKUP] ([IDLookup], [IDCurrentStatus], [IDParent], [Name], [Code], [CreateDate], [CreateUser]) VALUES (2, 2, 1, N'CURRENT_STATUS_ACTIVE', N'Aktif', CAST(N'2020-05-21 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[tLOOKUP] ([IDLookup], [IDCurrentStatus], [IDParent], [Name], [Code], [CreateDate], [CreateUser]) VALUES (3, 2, 1, N'CURRENT_STATUS_SUSPENDED', N'Askıda', CAST(N'2020-05-21 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[tLOOKUP] ([IDLookup], [IDCurrentStatus], [IDParent], [Name], [Code], [CreateDate], [CreateUser]) VALUES (4, 2, 1, N'CURRENT_STATUS_DELETED', N'Silinmiş', CAST(N'2020-05-21 00:00:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tLOOKUP] OFF
SET IDENTITY_INSERT [dbo].[tUSER] ON 

INSERT [dbo].[tUSER] ([IDUser], [IDCurrentStatus], [Name], [Surname], [Token], [UserName], [Password]) VALUES (2, 2, N'Bayram', N'Kaya', N'eyJhbGciOiJQUzM4NCIsInR5cCI6IkpXVCJ9', N'bayram', N'kaya')
SET IDENTITY_INSERT [dbo].[tUSER] OFF
